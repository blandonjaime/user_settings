import 'package:flutter/material.dart';
import 'package:user_settings/src/pages/home_page.dart';
import 'package:user_settings/src/pages/settings_page.dart';
import 'package:user_settings/src/share_prefs/preferencias_usuario.dart';
 
void main() async { 

  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPref();

  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {

  //static final String routeName = 'home';

  @override
  Widget build(BuildContext context) {

    final prefs = new PreferenciasUsuario();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'User Settings',
      initialRoute: prefs.ultimaPagina,
      routes: {
        HomePage.routeName        :     (BuildContext context) => HomePage(),
        SettingsPage.routeName    :     (BuildContext context) => SettingsPage(),
      },
    );
  }
}