
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_settings/src/pages/home_page.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia  = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPref() async {
    _prefs = await SharedPreferences.getInstance();
  }

  //None of this props is used.
  bool _colorSecundario;
  int _genero;
  String _nombre;

  // GETTERS SETTERS

  get genero {
    return _prefs.getInt('genero') ?? 1;
  }

  set setGenero(int value){
    _prefs.setInt('genero', value);
  }

  get colorSecundario {
    return _prefs.getBool('colorSecundario') ?? false;
  }

  set colorSecundario(bool value){
    _prefs.setBool('colorSecundario', value);
  }

  get nombreUsuario {
    return _prefs.getString('nombreUsuario') ?? '';
  }

  set nombreUsuario(String value){
    _prefs.setString('nombreUsuario', value);
  }

  get ultimaPagina {
    return _prefs.getString('ultimaPagina') ?? HomePage.routeName;
  }

  set ultimaPagina(String value){
    _prefs.setString('ultimaPagina', value);
  }

}