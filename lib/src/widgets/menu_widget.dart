import 'package:flutter/material.dart';
import 'package:user_settings/src/pages/home_page.dart';
import 'package:user_settings/src/pages/settings_page.dart';

class MenuWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            color: Colors.blueAccent,
            padding: EdgeInsets.zero,
            child: DrawerHeader(
              //margin: EdgeInsets.zero,
              padding: EdgeInsets.zero,
              child: Image(
                image: AssetImage('assets/img/menu-img.jpg'),
                fit: BoxFit.cover,
              ),
              //child: Container(
              //  decoration: BoxDecoration(
              //    image: DecorationImage(
              //      image: AssetImage('assets/img/menu-img.jpg'),
              //      fit: BoxFit.fitHeight,
              //    ),
              //    color: Colors.blue,
              //  ),
              ),
            ),
          //),

          ListTile(
            leading: Icon(Icons.pages, color: Colors.blue),
            title: Text('Home'),
            onTap: (){
              Navigator.pushReplacementNamed(context, HomePage.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.party_mode, color: Colors.blue),
            title: Text('Party Mode'),
            onTap: (){},
          ),
          ListTile(
            leading: Icon(Icons.people, color: Colors.blue),
            title: Text('People'),
            onTap: (){},
          ),
          ListTile(
            leading: Icon(Icons.settings, color: Colors.blue),
            title: Text('Settings'),
            onTap: (){
              //Navigator.pop(context);
              Navigator.pushReplacementNamed(context, SettingsPage.routeName);
            },
          ),

        ],
      ),
    );
  }
}