import 'package:flutter/material.dart';
import 'package:user_settings/src/share_prefs/preferencias_usuario.dart';
import 'package:user_settings/src/widgets/menu_widget.dart';

class HomePage extends StatelessWidget {

  static final routeName = 'home';
  final prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {

    prefs.ultimaPagina = HomePage.routeName;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: (prefs.colorSecundario) ? Colors.teal : Colors.blue,
        title: Text('Preferencias de Usuario'),
      ),
      drawer: MenuWidget(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Color Secundario: ${ prefs.colorSecundario }'),
          Divider(),
          Text('Género: ${ prefs.genero }'),
          Divider(),
          Text('Nombre Usuario: ${ prefs.nombreUsuario }'),
          Divider(),
        ],
      ),
    );
  }

}