import 'package:flutter/material.dart';
//import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_settings/src/share_prefs/preferencias_usuario.dart';
import 'package:user_settings/src/widgets/menu_widget.dart';

class SettingsPage extends StatefulWidget {

  static final routeName = 'settings';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  bool _colorSecundario;
  int _genero;
  String _nombre;

  TextEditingController _textController;

  final prefs = new PreferenciasUsuario();

  @override
  void initState() {
    super.initState();
    prefs.ultimaPagina = SettingsPage.routeName;
    _genero = prefs.genero;
    _colorSecundario = prefs.colorSecundario;
    //cargarPref();

    _textController = new TextEditingController(
      text: prefs.nombreUsuario
    );

  }

  // cargarPref() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   _genero = prefs.getInt('genero');
  //   setState(() {
  //   });
  // }

  _setSelectedRadio( int valor ) {

    // SharedPreferences prefs = await SharedPreferences.getInstance();

    // await prefs.setInt('genero', valor);
    prefs.setGenero = valor;

    setState(() {
      _genero = valor;
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Ajustes'),
        backgroundColor: (prefs.colorSecundario) ? Colors.teal : Colors.blue,
      ),
      drawer: MenuWidget(),
      body: ListView(
        children: <Widget>[
          
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text('Settings',
              style: TextStyle(fontSize: 45.0,
                fontWeight: FontWeight.bold
              ),
            ),
          ),    
          
          Divider(),

          SwitchListTile(
            value: _colorSecundario,
            title: Text('Color Secundario'),
            onChanged: ( valor ){
              setState(() {
                prefs.colorSecundario = valor;
                _colorSecundario = valor;
              });
            },
          ),
          
          RadioListTile(
            value: 1,
            title: Text('Masculino'),
            groupValue: _genero,
            onChanged: _setSelectedRadio,
          ),

          RadioListTile(
            value: 2,
            title: Text('Femenino'),
            groupValue: _genero,
            onChanged: _setSelectedRadio,
          ),

          Divider(),

          Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                labelText: 'Nombre',
                helperText: 'Nombre de la persona',
              ),
              onChanged: (value){
                prefs.nombreUsuario = value;
              },
            ),
          )

        ],
      )
    );
  }
}